cert hierarchy
level 1
subject: Ken
issuer: Ken

Level 2
subject: 1001 (prefered with serial number)
issuer: ken

Level 3
subject: tim
issuer: 1001

Level 4
subject: chet
issuer: time

Report
x509v3 exension vs x509v1 using common name

- benefit of using common name is that it would be passed down in the certificate hierarchy whereas arbitary extension don't.
- However by using common name it does not represent its original purpose
eg. CN = MobileMinute, CN = MM, CN = 10, etc..
- Using our own extensions (arbitrary extension) would serve its own purpose but the drawback is that we can't show the extension information with regular certificate tool.


Future development
==================
- add browse function for user to import csr file as well as the option to choose from list of users using the web services


certificate

- add "registrar" extension for cert creation
- how to sign "payee's CSR" with "token's private key"
- how to update a data? put() doesn't seem to update if there is already a value in the attribute (delete and recreate)
- can i use my csr private key to create a self-sign certificate?
- line 363 in main.py - how to go to next line for saving the certificate ("\n" does not work, while <br> is inappropriate)
- how to export file (where it automatically show "download")


found problem - reported by ken
================================================================================
Certificate signing requests (CSR) do not include a private key but are generated with a private key. The certificate signing request for a token is an internal object for a currency issuer. The current implementation stores the currency issuer private key on the server which exposes it to the server owner. A future model would be to create the token CSR on a server but transmit the CSR to a machine controlled by the issuer where the token is signed so as the private key remains private to the issuer. 

In this model the private key for the token will be known to the server so that the server can sign over the token to the first recipient but the currency issuer private key will remain unknown to the server. That is; the server can hand over the token but can't create it.
================================================================================

================================================================================
You can sign in the token creator but their is no facility to sign out. Ideally you could sign out or it would time out after half an hour or so.
================================================================================